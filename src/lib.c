#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <lib.h>

#define SECRET_VALUE "1442342"

void hash(char *s, int length)
{
    // maybe we're using this for some secret
    char *SECRET_SEED = malloc(sizeof(char) * STRING_SIZE);
    strcpy(SECRET_SEED, SECRET_VALUE);

    // encryption algorithm
    for(int i = 0; i < strlen(s); i++) {
        s[i] = ((s[i] + atoi(SECRET_SEED))  % 122) + 65;
    }

    // print encrypted text to user
    puts("");
    write(1, s, length);
    free(SECRET_SEED);
}

// trusting user input
int echo()
{
    char x[STRING_SIZE];
    long int n;
    printf("Enter a string\n");
    fgets(x, STRING_SIZE, stdin);

    printf("How big is your string?\n");
    scanf("%ld", &n);

    int written = write(1, x, n);

    return written;
}

// heartbleed style trusting length arg
char* copy_to_new_buffer(char *s, int length)
{
    char secret[] = "secret stuff";
    char buffer[STRING_SIZE];
    char *ret = malloc(sizeof(char) * length);

    strcpy(buffer, s);
    memcpy(ret, buffer, length);

    return ret;
}
