FROM debian:buster

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y gcc clang make clang-tools perl patch

COPY src /root/program/src
COPY include /root/program/include
COPY test /root/program/test
COPY run.sh /root/program
WORKDIR /root/program

# Apply a patch to scan-build fixing -V
# Disgusting patch, but it's just a workaround
# since nothing official exists
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=941614
COPY files/scan-build.patch /root
RUN patch $(readlink -f $(which scan-build)) < /root/scan-build.patch

EXPOSE 8181
CMD [ "/root/program/run.sh" ]
