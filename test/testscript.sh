#!/bin/bash
source secret.sh

# Set ASCII escape codes into variables
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

check_status() {
    echo -n "$1"
    if [ $2 -eq 0 ]; then
        echo -e  "${GREEN}PASS${NC}"

    else
        echo -e "${RED}FAILED${NC}"
    fi
}

# Test 1; testing echo()
echo
echo -e 'lorem\n5' | ./test_echo >/dev/null 2>&1
check_status "Testing echo(): String: 'lorem', Length: 5                        " $?

echo -e 'lorem\n21' | ./test_echo >/dev/null 2>&1
check_status "Testing echo(): String: 'lorem', Length: 21                       " $?

echo -e 'lorem\n100' | ./test_echo >/dev/null 2>&1
check_status "Testing echo(): String: 'lorem', Length: 100                      " $?

# Test 2; testing copy_to_new_buffer()
echo
./test_copy_to_new_buffer lorem 5 >/dev/null 2>&1
check_status "Testing copy_to_new_buffer(): String: 'lorem', Length: 5          " $?

./test_copy_to_new_buffer lorem 6 >/dev/null 2>&1
check_status "Testing copy_to_new_buffer(): String: 'lorem', Length: 6          " $?

./test_copy_to_new_buffer lorem 30 >/dev/null 2>&1
check_status "Testing copy_to_new_buffer(): String: 'lorem', Length: 30         " $?

# Test 3; testing hash()
echo
./test_hash lorem 5 >/dev/null 2>&1
check_status "Testing hash(): String: 'lorem', Length: 5                        " $?

./test_hash lorem 6 >/dev/null 2>&1
check_status "Testing hash(): String: 'lorem', Length: 6                        " $?

./test_hash lorem 30 >/dev/null 2>&1
check_status "Testing hash(): String: 'lorem', Length: 30                       " $?

echo && exit 0
