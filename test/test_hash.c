#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <lib.h>

int main(int argc, char** argv)
{
    int ret,arg2 = atoi(argv[2]);
    char *inStr = malloc(sizeof(char) * STRING_SIZE);
    strcpy(inStr, argv[1]);
    hash(inStr,arg2);

    ret = strcmp(inStr,"mpsfn");

    free(inStr);

    return ret;
}