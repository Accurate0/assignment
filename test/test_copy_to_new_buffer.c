#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <lib.h>

int main(int argc, char** argv)
{
    int arg2 = atoi(argv[2]);
    char *s = copy_to_new_buffer(argv[1],arg2);

    int ret = strcmp(s, argv[1]);

    write(1, s, arg2);

    free(s);

    return ret;
}
