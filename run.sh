#!/usr/bin/env bash
set -e

# Clear all the docker stuff
TERM=xterm-256color clear

echo '==> Unit Tests (without ASAN)'
make -C src/ >/dev/null
make -C test/ >/dev/null
(cd test; make test_script)


echo '==> Unit Tests (with ASAN)'
(make -C src/ clean; make -C src/ ASAN=1) >/dev/null
(make -C test/ clean; make -C test/ ASAN=1) >/dev/null
(cd test; make test_script)

if [ -z "$CI" ]; then
    echo '==> Static Analysis (scan-build)'
    echo '==> Available at: http://localhost:8080/'
    scan-build -V make -s -B -C src/
fi
